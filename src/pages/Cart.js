import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import CartInPage from "../components/CartInPage";

class Cart extends Component {
  render() {
    return (
      <div class="cart-area pb60">
        <div class="container">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="form-title">
                <h1>Shopping Cart</h1>
              </div>
              <div class="panel-body cart-body">
                <div class="table-responsive">
                  <table class="table">
                    <thead>
                      <tr>
                        <td class="name">Product Name</td>
                        <td>Model</td>
                        <td>Unit Price</td>
                        <td class="quantity">Quantity</td>
                        <td class="rem-up"></td>
                        <td>Total</td>
                      </tr>
                    </thead>
                    <tbody>
                      {this.props.carts.map((item, index) => (
                        <CartInPage item={item} index={index} />
                      ))}
                    </tbody>
                  </table>
                </div>
                <div
                  class="panel-group cart-acc"
                  id="accordion"
                  role="tablist"
                  aria-multiselectable="true"
                >
                  <div class="row">
                    <div class="panel col-md-4 col-sm-12 col-xs-12  super-accordion2">
                      <div class="panel-heading" role="tab" id="headingone">
                        <h4 class="panel-title">
                          <a
                            class="collapsed"
                            data-toggle="collapse"
                            data-parent="#accordion"
                            href="#coupon-code"
                            aria-expanded="false"
                            aria-controls="coupon-code"
                          >
                            Use Coupon Code
                            <span>
                              <i class="fa fa-caret-down"></i>
                            </span>
                          </a>
                        </h4>
                      </div>
                      <div
                        id="coupon-code"
                        class="panel-collapse collapse"
                        role="tabpanel"
                        aria-labelledby="headingone"
                      >
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-sm-12">
                              <fieldset id="coupne">
                                <div class="form-group required">
                                  <input
                                    type="text"
                                    class="form-control"
                                    id="input-coupne-code"
                                    placeholder="Coupne Code"
                                    value=""
                                    name="coupne-code"
                                  />
                                </div>
                              </fieldset>
                            </div>
                          </div>
                          <div class="buttons common-btn">
                            <input
                              type="button"
                              class="btn btn-primary"
                              data-loading-text="Loading..."
                              value="Continue"
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="panel col-md-4 col-sm-12 col-xs-12 super-accordion2">
                      <div class="panel-heading" role="tab" id="headingtwo">
                        <h4 class="panel-title">
                          <a
                            class="collapsed"
                            data-toggle="collapse"
                            data-parent="#accordion"
                            href="#gift-voucer"
                            aria-expanded="false"
                            aria-controls="coupon-code"
                          >
                            Use giftvoucer
                            <span>
                              <i class="fa fa-caret-down"></i>
                            </span>
                          </a>
                        </h4>
                      </div>
                      <div
                        id="gift-voucer"
                        class="panel-collapse collapse"
                        role="tabpanel"
                        aria-labelledby="headingtwo"
                      >
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-sm-12">
                              <fieldset id="gift">
                                <div class="form-group required">
                                  <input
                                    type="text"
                                    class="form-control"
                                    id="input-gift-voucer"
                                    placeholder="Gift Voucer"
                                    value=""
                                    name="gift-voucer"
                                  />
                                </div>
                              </fieldset>
                            </div>
                          </div>
                          <div class="buttons common-btn">
                            <input
                              type="button"
                              class="btn btn-primary"
                              data-loading-text="Loading..."
                              value="Continue"
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="panel col-md-4 col-sm-12 col-xs-12 super-accordion2">
                      <div class="panel-heading" role="tab" id="headingthree">
                        <h4 class="panel-title">
                          <a
                            class="collapsed"
                            data-toggle="collapse"
                            data-parent="#accordion"
                            href="#shipping-tax"
                            aria-expanded="false"
                            aria-controls="coupon-code"
                          >
                            Estimate Shipping & Taxes{" "}
                            <span>
                              <i class="fa fa-caret-down"></i>
                            </span>
                          </a>
                        </h4>
                      </div>
                      <div
                        id="shipping-tax"
                        class="panel-collapse collapse"
                        role="tabpanel"
                        aria-labelledby="headingthree"
                      >
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-sm-12">
                              <fieldset id="shipping-estimate">
                                <div class="form-group required">
                                  <span>
                                    Enter your destination to get a shipping
                                    estimate.
                                  </span>
                                  <label
                                    for="input-payment-country"
                                    class="control-label"
                                  >
                                    Country
                                  </label>
                                  <select
                                    class="form-control"
                                    id="input-payment-country"
                                    name="country_id"
                                  >
                                    <option value="">
                                      {" "}
                                      --- Please Select ---{" "}
                                    </option>
                                    <option value="244">Aaland Islands</option>
                                    <option value="1">Afghanistan</option>
                                    <option value="2">Albania</option>
                                    <option value="3">Algeria</option>
                                    <option value="4">American Samoa</option>
                                    <option value="5">Andorra</option>
                                    <option value="6">Angola</option>
                                    <option value="7">Anguilla</option>
                                    <option value="8">Antarctica</option>
                                    <option value="9">
                                      Antigua and Barbuda
                                    </option>
                                    <option value="213">
                                      Trinidad and Tobago
                                    </option>
                                    <option value="255">
                                      Tristan da Cunha
                                    </option>
                                  </select>
                                </div>
                                <div class="form-group required">
                                  <label
                                    for="input-payment-zone"
                                    class="control-label"
                                  >
                                    Region / State
                                  </label>
                                  <select
                                    class="form-control"
                                    id="input-payment-zone"
                                    name="zone_id"
                                  >
                                    <option value="3569">Monmouthshire</option>
                                    <option value="3570">Moray</option>
                                    <option value="3591">Somerset</option>
                                    <option value="3592">South Ayrshire</option>
                                    <option value="3593">
                                      South Lanarkshire
                                    </option>
                                    <option value="3603">Warwickshire</option>
                                    <option value="3604">
                                      West Dunbartonshire
                                    </option>
                                    <option value="3605">West Lothian</option>
                                    <option value="3606">West Midlands</option>
                                    <option value="3607">West Sussex</option>
                                    <option value="3608">West Yorkshire</option>
                                    <option value="3609">Western Isles</option>
                                    <option value="3610">Wiltshire</option>
                                    <option value="3611">Worcestershire</option>
                                    <option value="3612">Wrexham</option>
                                  </select>
                                </div>
                                <div class="form-group required">
                                  <label
                                    for="input-payment-postcode"
                                    class="control-label"
                                  >
                                    Post Code
                                  </label>
                                  <input
                                    type="text"
                                    class="form-control"
                                    id="input-payment-postcode"
                                    placeholder="Post Code"
                                    value=""
                                    name="postcode"
                                  />
                                </div>
                              </fieldset>
                            </div>
                          </div>
                          <div class="buttons common-btn">
                            <input
                              type="button"
                              class="btn btn-primary"
                              data-loading-text="Loading..."
                              value="Continue"
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tfoot cart-total mb50">
                  <table>
                    <tbody>
                      <tr>
                        <td class="left">
                          <b>Sub-Total:</b>
                        </td>
                        <td class="right">$168</td>
                      </tr>
                      <tr>
                        <td class="left ">
                          <b>Eco Tax (-2.00):</b>
                        </td>
                        <td class="right ">$2</td>
                      </tr>
                      <tr>
                        <td class="left ">
                          <b>VAT (20%):</b>
                        </td>
                        <td class="right ">$5</td>
                      </tr>
                      <tr>
                        <td class="left  last">
                          <b>Total:</b>
                        </td>
                        <td class="right  last">{this.props.totalPay}</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div class="btns-cart">
                  <div class="buttons common-btn btn3">
                    <input
                      type="button"
                      value="Continue Shopping"
                      data-loading-text="Loading..."
                      class="btn btn-primary c-btn"
                    />
                  </div>
                  <div class="buttons common-btn">
                    <input
                      type="button"
                      value="Confirm Order"
                      data-loading-text="Loading..."
                      class="btn btn-primary c-btn"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Cart.defaultProps = {
  carts: [],
  totalPay: 0,
};
//tao component ProductCard, dua component vao list

const mapStateToProps = (state) => ({
  carts: state.carts,
  totalPay: state.totalPay,
});

const mapDispatchToProps = (dispatch) => ({
  ...bindActionCreators({}, dispatch),
});
export default connect(mapStateToProps, mapDispatchToProps)(Cart);
