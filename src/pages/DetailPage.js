import React, { Component } from "react";
import { connect } from "react-redux";
import queryString from "query-string";
import { bindActionCreators } from "redux";
import { getSendoProduct } from "../actions";

class DetailPage extends Component {
  componentDidMount() {
    const parsed = queryString.parse(window.location.search);
    this.props.getSendoProduct(parsed.id);
  }
  render() {
    const { product } = this.props;

    return (
      <div>
        {product && <img src={product.images[0]} alt={product.name} />}

        {product && <p>Tên sản phẩm: {product.name} </p>}
        {product && <p>Tình trạng: {product.status_text} </p>}
        {product && <p>Giá: {product.price} </p>}
        {/* {index && <p>index {index}</p>}
        {console.log(index)} */}
      </div>
    );
  }
}

DetailPage.defaultProps = {
  product: undefined
};
//tao component ProductCard, dua component vao list

const mapStateToProps = state => ({
  product: state.product,
  index: state.index
});

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators({ getSendoProduct }, dispatch)
});
export default connect(mapStateToProps, mapDispatchToProps)(DetailPage);
