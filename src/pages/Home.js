import React, { Component } from "react";
import Slider from "../components/Slider";
import BigBanner from "../components/BigBanner";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getProducts } from "../actions";
import BestDeal from "../components/BestDeal";
import ListProducts from "./ListProducts";

class Home extends Component {
  componentDidMount() {
    this.props.getProducts("", 1, true);
  }
  render() {
    return (
      <div>
        <Slider />
        <BigBanner></BigBanner>
        <ListProducts />
      </div>
    );
  }
}

Home.defaultProps = {
  products: [],
  category: "",
  index: -1,
  errMsg: "",
  status: "",
  page: 1,
  query: ""
};
//tao component ProductCard, dua component vao list

const mapStateToProps = state => ({
  products: state.products,
  category: state.category,
  index: state.index,
  errMsg: state.errMsg,
  status: state.status,
  page: state.page,
  query: state.query
});

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators({ getProducts }, dispatch)
});
export default connect(mapStateToProps, mapDispatchToProps)(Home);
