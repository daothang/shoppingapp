import React, { Component } from "react";
import { login } from "../actions.js";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

export class AuthPage extends Component {
  state = {
    username: "",
    password: ""
  };

  handleChangeUser = event => {
    this.setState({ username: event.target.value });
  };
  handleChangePass = event => {
    this.setState({ password: event.target.value });
  };
  handleSubmit = event => {
    event.preventDefault();
    this.props.login(this.state.username, this.state.password);
    alert(JSON.stringify(this.state));
  };
  render() {
    return (
      <div>
        <h1>Login form</h1>
        <form onSubmit={this.handleSubmit}>
          <input
            type="text"
            placeholder="username"
            onChange={this.handleChangeUser}
          />
          <input
            type="text"
            placeholder="password"
            onChange={this.handleChangePass}
          />
          <button>login</button>
        </form>
      </div>
    );
  }
}
const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators({ login }, dispatch)
});
export default connect(mapStateToProps, mapDispatchToProps)(AuthPage);
