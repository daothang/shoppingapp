import React, { Component } from "react";
import queryString from "query-string";
import SingleProduct from "../components/SingleProduct";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { saveToQuery, getProducts, filter } from "../actions";
import Select from "react-select";

const options = [
  { value: 100000, label: "Giá từ 0 - 100.000đ" },
  { value: 200000, label: "Giá từ 0 - 200.000đ" },
  { value: 500000, label: "Giá từ 0 - 500.000đ" },
  { value: 1000000, label: "Giá từ 0 - 1.000.000đ" },
  { value: 2000000, label: "Giá từ 0 - 2.000.000đ" },
];
class ListProducts extends Component {
  handleChange = (selectedOption) => {
    this.props.filter({ selectedOption });
    console.log(`Option selected:`, selectedOption);
  };

  componentDidMount() {
    const parsed = queryString.parse(window.location.search);
    if (parsed.keywork != "") {
      this.props.getProducts(parsed.keywork, 1, true);
      this.props.saveToQuery(parsed.keywork);
    }
  }
  handleLoadMore = () => {
    let page = this.props.page;
    page++;
    this.props.getProducts(this.props.query, page);
  };
  render() {
    const { errMsg, status } = this.props;
    return (
      <div>
        {status == "START" && <p>Loading</p>}
        <h4>{this.props.errMsg}</h4>
        <div className="container">
          <div className="row">
            <div className="col-md-12 col-sm-12 col-xs-12">
              <div className="content_bg">
                <div className="category-info">
                  <h2>Filter by price</h2>
                  <div className="info_detail">
                    <Select options={options} onChange={this.handleChange} />

                    <p>
                      Nam tristique porta ligula, vel viverra sem eleifend nec.
                      Nulla sed purus augue, eu euismod tellus. Nam mattis eros
                      nec mi sagittis sagittis.
                      <span>
                        Vestibulum suscipit cursus bibendum. Integer at justo
                        eget sem auctor auctor eget vitae arcu. Nam tempor
                        malesuada porttitor. Nulla quis dignissim ipsum. Aliquam
                        pulvinar iaculis justo, sit amet interdum sem hendrerit
                        vitae. Vivamus vel erat tortor. Nulla facilisi. In nulla
                        quam, lacinia eu aliquam ac, aliquam in nisl.
                      </span>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="shop-list-grid-area">
          <div className="container">
            <div className="product-filter">
              <div className="col-md-6 col-sm-6 col-xs-12">
                <div className="sort">
                  <label> Sort By:</label>
                </div>
                <div className="compare-total">
                  <a id="compare-total" href="#">
                    Product Compare (0)
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="shop-product-area pt30 ">
          <div className="tab-content">
            <div className="container">
              <div class="row row-padd">
                {this.props.products.map((item, index) => (
                  <SingleProduct
                    product={item}
                    index={index}
                    isSeleted={this.props.index == index}
                  />
                ))}
              </div>
            </div>
          </div>
        </div>

        <button type="button" onClick={this.handleLoadMore}>
          Load more
        </button>
      </div>
    );
  }
}
ListProducts.defaultProps = {
  products: [],
  category: "",
  index: -1,
  errMsg: "",
  status: "",
  page: 1,
  query: "",
};
//tao component ProductCard, dua component vao list

const mapStateToProps = (state) => ({
  products: state.products,
  category: state.category,
  index: state.index,
  errMsg: state.errMsg,
  status: state.status,
  page: state.page,
  query: state.query,
});

const mapDispatchToProps = (dispatch) => ({
  ...bindActionCreators({ saveToQuery, getProducts, filter }, dispatch),
});
export default connect(mapStateToProps, mapDispatchToProps)(ListProducts);
