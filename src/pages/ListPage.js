import React, { Component } from "react";
import ProductCard from "../components/ProductCard.js";
import SingleProduct from "../components/SingleProduct";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { filter, getProducts } from "../actions";
import SearchBox from "../components/SearchBox.js";
import SearchBar from "../components/SearchBar";

class ListPage extends Component {
  handleClick() {
    console.log("Click happened");
  }
  handleSearch = event => {
    event.preventDefault();
    this.props.getProducts(this.state.query, 1, true);
  };
  handleChange = event => {
    this.setState({ query: event.target.value });
  };

  renderSwitch(param) {
    switch (param) {
      case "SUCCESS":
        return (
          <div>
            <h4>{this.props.errMsg}</h4>
            Selected : {this.props.index}
            <button type="button" onClick={() => this.props.filter("apple")}>
              APPLE
            </button>
            <button type="button" onClick={() => this.props.filter("samsung")}>
              SAMSUNG
            </button>
            <p>Selected Category : {this.props.category}</p>
            <div className="container">
              {this.props.products.map((item, index) => (
                <SingleProduct
                  product={item}
                  index={index}
                  isSeleted={this.props.index == index}
                />
              ))}
            </div>
          </div>
        );
      case "START":
        return (
          <div>
            "VUI LONG CHO"
            <br />
            <img src="/giphy.webp"></img>
          </div>
        );
      default:
        return "foo";
    }
  }
  handleLoadMore = () => {
    let page = this.props.page;
    page++;
    this.props.getProducts(this.props.query, page);
  };
  render() {
    const { errMsg, status } = this.props;
    return (
      <div>
        {/* <form onSubmit={this.handleSearch}>
          <input
            type="text"
            value={this.state.query}
            onChange={this.handleChange}
          />
          <input type="submit" value="request" />
        </form> */}
        {status == "START" && <p>Loading</p>}
        <h4>{this.props.errMsg}</h4>
        Selected : {this.props.index}
        <button type="button" onClick={() => this.props.filter("apple")}>
          APPLE
        </button>
        <button type="button" onClick={() => this.props.filter("samsung")}>
          SAMSUNG
        </button>
        <p>Selected Category : {this.props.category}</p>
        <div class="row row-padd">
          {this.props.products.map((item, index) => (
            <SingleProduct
              product={item}
              index={index}
              isSeleted={this.props.index == index}
            />
          ))}
        </div>
        <button type="button" onClick={this.handleLoadMore}>
          Load more
        </button>
      </div>
    );
  }
}
ListPage.defaultProps = {
  products: [],
  category: "",
  index: -1,
  errMsg: "",
  status: "",
  page: 1,
  query: ""
};
//tao component ProductCard, dua component vao list

const mapStateToProps = state => ({
  products: state.products,
  category: state.category,
  index: state.index,
  errMsg: state.errMsg,
  status: state.status,
  page: state.page,
  query: state.query
});

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators({ filter, getProducts }, dispatch)
});
export default connect(mapStateToProps, mapDispatchToProps)(ListPage);
