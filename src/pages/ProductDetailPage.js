import React, { Component } from "react";
import { connect } from "react-redux";
import queryString from "query-string";
import { bindActionCreators } from "redux";
import { addCart, getSendoProduct } from "../actions";
class ProductDetailPage extends Component {
  componentDidMount() {
    const parsed = queryString.parse(window.location.search);
    this.props.getSendoProduct(parsed.id);
  }
  addToCart = (id) => {
    this.props.addCart(id);
  };
  render() {
    const { product } = this.props;
    const flexImg = {
      display: "flex",
      flexwrap: "wrap",
    };
    return (
      <div className="single-product-wraper">
        <div className="single-prodcut-area">
          {product && (
            <div>
              <div className="container">
                <div className="row row-padd ">
                  <div className="col-md-5 col-sm-6 col-xs-12 col-padd">
                    <div className="product-zoom-area">
                      <div className="imgs-area">
                        <img
                          id="zoom_03"
                          src={product.images[0]}
                          data-zoom-image={product.images[0]}
                          alt=""
                        />
                        <div className="row">
                          <div className="col-md-12 col-sm-12 col-xs-12">
                            <div className="im-gall">
                              <div id="gallery_01" style={flexImg}>
                                {product.images.map((item, index) => (
                                  <div className="p-c" key={index}>
                                    <a
                                      href="#"
                                      data-image={item}
                                      data-zoom-image="img/product/large/h9-680x573.jpg"
                                    >
                                      <img
                                        className="zoom_03"
                                        src={item}
                                        alt=""
                                      />
                                    </a>
                                  </div>
                                ))}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-7 col-sm-6 col-xs-12 col-padd">
                    <div className="single-prodcut-des">
                      <div className="product-name">
                        <h3>{product.name}</h3>
                      </div>
                      <div className="product-des">
                        <ul>
                          <li>
                            Thương hiệu: <a href="#"> {product.brand_name}</a>
                          </li>
                          <li>Tình trạng: {product.status_text}</li>
                          <li>Đã mua: {product.order_count}</li>
                          <li>Số lượng kho :{product.quantity}</li>
                        </ul>
                      </div>
                      <div className="social-bar">
                        <span>
                          <a href="#">
                            <i className="fa fa-envelope"></i>
                          </a>
                          Email me when available
                        </span>
                        <ul>
                          <li>
                            <a className="fbook" href="#" title="Facebook">
                              <i className="fa fa-facebook"></i>
                            </a>
                          </li>
                          <li>
                            <a
                              className="pinte"
                              href="#"
                              target="_blank"
                              title="Pinterest"
                            >
                              <i className="fa fa-pinterest"></i>
                            </a>
                          </li>
                          <li>
                            <a className="twitt" href="#" title="Tweet">
                              <i className="fa fa-twitter"></i>
                            </a>
                          </li>
                          <li>
                            <a
                              className="goog"
                              href="#"
                              target="_blank"
                              title="Google+"
                            >
                              <i className="fa fa-google-plus"></i>
                            </a>
                          </li>
                        </ul>
                      </div>
                      <div className="ratting-review">
                        <div className="ratting">
                          <i className="fa fa-star-o"></i>
                          <i className="fa fa-star-o"></i>
                          <i className="fa fa-star-o"></i>
                          <i className="fa fa-star-o"></i>
                          <i className="fa fa-star-o"></i>
                          <span>
                            <a href="#">0 review</a>
                          </span>
                        </div>
                        <div className="write-rev">
                          <a href="#">
                            <i className="fa fa-pencil"></i>Write a review
                          </a>
                        </div>
                      </div>
                      <div className="price-type">
                        <div className="running-price">
                          <span>{product.final_price}</span>
                        </div>
                        <div className="old-price">
                          <span>
                            <del>{product.price}</del>
                          </span>
                        </div>
                        <div className="price-tax">
                          <span>{product.final_price}</span>
                        </div>
                      </div>
                      <div className="quantity-area">
                        <label>Qty :</label>
                        <div className="cart-quantity">
                          <form action="#" method="POST" id="myform">
                            <div className="product-quantity">
                              <div className="cart-quantity">
                                <div className="cart-plus-minus">
                                  <div className="dec qtybutton">-</div>
                                  <input
                                    type="text"
                                    defaultValue="02"
                                    name="qtybutton"
                                    className="cart-plus-minus-box"
                                  />
                                  <div className="inc qtybutton">+</div>
                                </div>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                      <div className="cart-btn">
                        <a onClick={() => this.addToCart(product.id)}>
                          Add To Cart
                        </a>

                        <a title="add to wish list" className="wishl" href="#">
                          <i className="fa fa-heart"></i>
                        </a>
                        <a title="add to compare" className="wishl" href="#">
                          <i className="fa fa-retweet"></i>
                        </a>
                      </div>
                      <div className="count-bought">
                        <div className="time-counter">
                          <div data-countdown="2018/07/01"></div>
                        </div>
                        <div className="bought">
                          <span>11 Bought</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="dec-re-comm-area">
                <div className="container">
                  <div className="row">
                    <div className="col-md-12 col-sm-12 col-xs-12">
                      {/* Nav tabs  */}
                      <ul className="nav-tab" role="tablist">
                        <li role="presentation" className="active">
                          <a href="#descri" data-toggle="tab">
                            Description
                          </a>
                        </li>
                        <li role="presentation">
                          <a href="#revie" data-toggle="tab">
                            Reviews(0)
                          </a>
                        </li>
                        <li role="presentation">
                          <a href="#fac-com" data-toggle="tab">
                            Face Comments
                          </a>
                        </li>
                      </ul>
                      <div className="single-tab-area">
                        {/* Tab panes  */}
                        <div className="tab-content">
                          <div
                            role="tabpanel"
                            className="tab-pane fade in active"
                            id="descri"
                          >
                            <div className="tab-descripton">
                              <h2>Details</h2>
                              <h4>Nations fundraising campaign meaningful </h4>
                              <img src="img/product/desc_product.jpg" alt="" />
                              <div className="product-info">
                                <h2>Product information</h2>
                                <ul>
                                  <li>Exceptionally lightweight</li>
                                  <li>Air Clean filter</li>
                                  <li>
                                    Dusting brush, upholstery tool and crevice
                                    nozzle on VarioClip
                                  </li>
                                  <li>29.5-feet cleaning radius</li>
                                  <li>
                                    Two floor tools included: Turbo Comfort
                                    turbobrush, ideal for low to medium pile
                                    carpeting, area rugs and all smooth
                                    flooring; and a Parquet floor tool for the
                                    gentle cleaning of smooth surfaces
                                  </li>
                                </ul>
                              </div>
                              <div className="descripton-img">
                                <div className="left-img">
                                  <img src="img/product/desc_1.jpg" alt="" />
                                </div>
                                <div className="right-img">
                                  <img src="img/product/desc_2.jpg" alt="" />
                                </div>
                              </div>
                              <div className="product-des1">
                                <p>
                                  Equality asylum fundraising campaign
                                  policymakers, making progress country protect
                                  cross-agency coordination. Recognize potential
                                  participatory monitoring, beneficiaries,
                                  activism livelihoods public sector cooperation
                                  revitalize social. Justice agency,
                                  cross-cultural overcome injustice John Lennon.
                                  Giving gender youth social responsibility
                                  fluctuation, compassion catalytic effect rural
                                  development fairness. Carbon emissions
                                  reductions positive social change, fellows
                                  sustainable future indicator lasting change
                                  donation.{" "}
                                </p>
                                <p className="pd3">
                                  Civil society Andrew Carnegie growth
                                  insurmountable challenges sanitation community
                                  health workers forward-thinking. Global
                                  citizens; Millennium Development Goals;
                                  connect prevention education policy dialogue
                                  fighting poverty. Jane Jacobs, worldwide
                                  institutions peaceful; vaccines, conflict
                                  resolution crisis situation working alongside.
                                  Innovation, fight against oppression
                                  progressive refugee effectiveness. Social
                                  good.{" "}
                                </p>
                              </div>
                              <div className="descripton-img">
                                <div className="left-img">
                                  <img src="img/product/desc_3.jpg" alt="" />
                                </div>
                                <div className="right-img">
                                  <img src="img/product/desc_4.jpg" alt="" />
                                </div>
                              </div>
                              <div className="product-des1">
                                <p>
                                  Equality asylum fundraising campaign
                                  policymakers, making progress country protect
                                  cross-agency coordination. Recognize potential
                                  participatory monitoring, beneficiaries,
                                  activism livelihoods public sector cooperation
                                  revitalize social. Justice agency,
                                  cross-cultural overcome injustice John Lennon.
                                  Giving gender youth social responsibility
                                  fluctuation, compassion catalytic effect rural
                                  development fairness. Carbon emissions
                                  reductions positive social change, fellows
                                  sustainable future indicator lasting change
                                  donation.{" "}
                                </p>
                                <p className="pd3">
                                  Civil society Andrew Carnegie growth
                                  insurmountable challenges sanitation community
                                  health workers forward-thinking. Global
                                  citizens; Millennium Development Goals;
                                  connect prevention education policy dialogue
                                  fighting poverty. Jane Jacobs, worldwide
                                  institutions peaceful; vaccines, conflict
                                  resolution crisis situation working alongside.
                                  Innovation, fight against oppression
                                  progressive refugee effectiveness. Social
                                  good.
                                </p>
                              </div>

                              <div className="descripton-img">
                                <div className="left-img">
                                  <img src="img/product/desc_5.jpg" alt="" />
                                  <span>
                                    Nations fundraising campaign meaningful
                                    <br /> White
                                  </span>
                                </div>
                                <div className="right-img">
                                  <img src="img/product/desc_6.jpg" alt="" />
                                  <span>
                                    Nations fundraising campaign meaningful
                                    <br />
                                    Brown
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div
                            role="tabpanel"
                            className="tab-pane fade in"
                            id="revie"
                          >
                            <div className="review-heading">
                              <h3>Reviews (0)</h3>
                            </div>
                            <div id="bt_review">
                              <form
                                id="form-review"
                                className="form-horizontal"
                              >
                                <div id="review">
                                  <p>There are no reviews for this product.</p>
                                </div>
                                <div className="review-heading">
                                  <h3>Write a review</h3>
                                </div>
                                <div className="form-group input-boxx">
                                  <div className="col-sm-12">
                                    <label
                                      htmlFor="input-name"
                                      className="control-label"
                                    >
                                      <b>
                                        Your Name<em>*</em>
                                      </b>
                                    </label>
                                    <input
                                      type="text"
                                      className="form-control"
                                      id="input-name"
                                      defaultValue=""
                                      name="name"
                                    />
                                  </div>
                                </div>
                                <div className="form-group input-boxx">
                                  <div className="col-sm-12">
                                    <label
                                      htmlFor="input-review"
                                      className="control-label"
                                    >
                                      <b>
                                        Your Review<em>*</em>
                                      </b>
                                    </label>
                                    <textarea
                                      className="form-control"
                                      id="input-review"
                                      rows="5"
                                      name="text"
                                    ></textarea>
                                    <p className="help-block">
                                      <span className="text-danger">Note:</span>{" "}
                                      HTML is not translated!
                                    </p>
                                  </div>
                                </div>
                                <div className="form-group form-common">
                                  <div className="col-sm-12">
                                    <label className="control-label label-rating">
                                      <b>
                                        Rating<em>*</em>
                                      </b>
                                    </label>
                                    Bad
                                    <input
                                      type="radio"
                                      defaultValue="1"
                                      name="rating"
                                    />
                                    <input
                                      type="radio"
                                      value="2"
                                      name="rating"
                                    />
                                    <input
                                      type="radio"
                                      value="3"
                                      name="rating"
                                    />
                                    <input
                                      type="radio"
                                      value="4"
                                      name="rating"
                                    />
                                    <input
                                      type="radio"
                                      value="5"
                                      name="rating"
                                    />
                                    Good
                                  </div>
                                </div>
                                <div className="buttons common-btn">
                                  <div className="pull-left">
                                    <button
                                      className="btn btn-primary"
                                      data-loading-text="Loading..."
                                      id="button-review"
                                      type="button"
                                    >
                                      Continue
                                    </button>
                                  </div>
                                </div>
                              </form>
                            </div>
                          </div>
                          <div
                            role="tabpanel"
                            className="tab-pane fade in"
                            id="fac-com"
                          >
                            <div className="facebook-comments-box">
                              <div className="f-comments">
                                <span>0 Comments</span>
                                <div className="sort">
                                  <label> Sort By:</label>
                                  <select id="input-sort">
                                    <option value="">Newest</option>
                                    <option value="">oldest</option>
                                  </select>
                                </div>
                              </div>
                              <div className="cmnt-box">
                                <a href="#">
                                  <img src="img/author.jpg" alt="" />
                                </a>
                                <textarea></textarea>
                              </div>
                              <div className="also-face">
                                <form action="#">
                                  <input
                                    type="checkbox"
                                    name="gender"
                                    value="male"
                                  />{" "}
                                  Also post on Facebook
                                </form>
                                <a href="#">Log in to post</a>
                              </div>
                              <span>
                                <i className="fa fa-facebook"></i>
                                <a href="#">Facebook Comments Plugin</a>
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

ProductDetailPage.defaultProps = {
  product: undefined,
};

const mapStateToProps = (state) => ({
  product: state.product,
});

const mapDispatchToProps = (dispatch) => ({
  ...bindActionCreators({ getSendoProduct, addCart }, dispatch),
});
export default connect(mapStateToProps, mapDispatchToProps)(ProductDetailPage);
