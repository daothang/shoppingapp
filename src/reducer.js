import {
  FILTER,
  GET_PRODUCT,
  GET_PRODUCTS,
  GET_PRODUCT_ID,
  GET_PRODUCTS_SUCCESS,
  GET_PRODUCTS_FAIL,
  GET_PRODUCTS_START,
  ADD_TO_CART,
  SAVE_TO_QUERY,
  INCREASE_ITEM,
  REMOVE_FROM_CART,
} from "./actions";
const initialState = {
  products: [],
  category: "",
  product: undefined,
  index: -1,
  errMsg: "",
  status: "START",
  page: 1,
  carts: [],
  value: 1,
  totalPay: 0,
  query: "",
  limitPrice: undefined,
};
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case REMOVE_FROM_CART:
      let tempCarts = state.carts;
      tempCarts.splice(action.index, 1);
      var totalPay = 0;
      tempCarts.map((item, index) => {
        totalPay += item.item.final_price * item.quanty;
      });
      return { ...state, carts: tempCarts, totalPay: totalPay };

    case INCREASE_ITEM:
      let listcarts = [...state.carts];
      for (var i = 0; i < listcarts.length; i++) {
        if (action.id === listcarts[i].item.id) {
          if (action.isIncrease) {
            listcarts[i].quanty++;
          } else {
            listcarts[i].quanty--;
          }
          break;
        }
      }
      var totalPay = 0;
      listcarts.map((item, index) => {
        totalPay += item.item.final_price * item.quanty;
      });

      return { ...state, carts: listcarts, totalPay: totalPay };
    case SAVE_TO_QUERY:
      return { ...state, query: action.query };
    case ADD_TO_CART:
      let itemCart = {
        quanty: 1,
        item: action.item,
      };
      console.log(itemCart);
      let carts = [...state.carts];

      if (carts.length > 0) {
        // the array is defined and has at least one element
        let isExits = false;
        for (var i = 0; i < carts.length; i++) {
          if (itemCart.item.id === carts[i].item.id) {
            console.log("loop " + i);
            carts[i].quanty++;
            isExits = true;
            break;
          }
        }
        if (!isExits) carts.push(itemCart);
      } else {
        carts.push(itemCart);
      }

      var totalPay = state.totalPay + action.item.final_price;
      return { ...state, carts: carts, totalPay: totalPay };
    case FILTER:
      return { ...state, limitPrice: action.category };
    case GET_PRODUCT:
      const product = state.products[action.index];
      return { ...state, product: product, index: action.index };
    case GET_PRODUCTS_START:
      return { ...state, products: [], status: "" };
    case GET_PRODUCTS_SUCCESS:
      const newProduct = [...state.products, ...action.products];
      return {
        ...state,
        products: action.isNewSearch ? action.products : newProduct,
        status: "SUCCESS",
        page: action.page,
      };
    case GET_PRODUCT_ID:
      const producta = action.sendoProduct;
      return { ...state, product: producta };
    case GET_PRODUCTS_FAIL:
      return { ...state, errMsg: action.errorMsg, status: "FAIL" };
    default:
      return state;
  }
};
export default reducer;
