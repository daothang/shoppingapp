import React, { Component } from "react";
import Cart from "./Cart";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { bindActionCreators } from "redux";
import SearchBar from "./SearchBar";

export class Header extends Component {
  totalMoney(carts) {
    let totalMoney = 0;
    carts.map((item, index) => (totalMoney += item.final_price));
    return totalMoney;
  }
  render() {
    return (
      <div>
        <div className="b_header_middle">
          <div className="container">
            <div className="row">
              <div className="top_left col-md-6 col-sm-6 col-xs-12 pull-left">
                <div className="language_currency">
                  <form className="currency" method="post" action="#">
                    <div className="btn-group">
                      <button
                        data-toggle="dropdown"
                        className="btn-link dropdown-toggle"
                      >
                        <strong>
                          <i className="fa fa-usd"></i>
                          <b>USD</b>
                        </strong>
                        <i className="fa fa-angle-down"></i>
                      </button>
                      <ul className="dropdown-menu">
                        <li>
                          <button
                            name="EUR"
                            type="button"
                            className="currency-select"
                          >
                            €<b> EUR</b>
                          </button>
                        </li>
                        <li>
                          <button
                            name="GBP"
                            type="button"
                            className="currency-select"
                          >
                            £<b> GBP</b>
                          </button>
                        </li>
                        <li>
                          <button
                            name="USD"
                            type="button"
                            className="currency-select"
                          >
                            $<b> USD</b>
                          </button>
                        </li>
                      </ul>
                    </div>
                    <input type="hidden" value="" name="code" />
                    <input type="hidden" value="" name="redirect" />
                  </form>
                  <form className="language" method="post" action="#">
                    <div className="btn-group">
                      <button
                        data-toggle="dropdown"
                        className="btn-link dropdown-toggle"
                      >
                        <strong>
                          <img
                            title="English"
                            alt="English"
                            src="img/icon/gb.png"
                          />
                          <b>Language</b>
                        </strong>
                        <i className="fa fa-angle-down"></i>
                      </button>
                      <ul className="dropdown-menu">
                        <li>
                          <a href="#">
                            <img
                              title="English"
                              alt="English"
                              src="img/icon/gb.png"
                            />
                            <span>English</span>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <img
                              title="Deutsch"
                              alt="Deutsch"
                              src="img/icon/de.png"
                            />
                            <span>Deutsch</span>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </form>
                </div>
                <div className="contact">
                  <i className="fa fa-phone"></i> <span>123 908 467</span>
                </div>
              </div>
              <div className="top_right col-md-6 col-sm-6 col-xs-12 pull-right">
                <div className="footer-social">
                  <ul>
                    <li className="facebook">
                      <a
                        data-toggle="tooltip"
                        data-placement="top"
                        data-original-title="Facebook"
                        href="#"
                      >
                        <i className="fa fa-facebook"></i>
                      </a>
                    </li>
                    <li className="twitter">
                      <a
                        data-toggle="tooltip"
                        data-placement="top"
                        data-original-title="Twitter"
                        href="#"
                      >
                        <i className="fa fa-twitter"></i>
                      </a>
                    </li>
                    <li className="googleplus">
                      <a
                        data-toggle="tooltip"
                        data-placement="top"
                        data-original-title="Googleplus"
                        href="#"
                      >
                        <i className="fa fa-google-plus"></i>
                      </a>
                    </li>
                    <li className="pinterest">
                      <a
                        data-toggle="tooltip"
                        data-placement="top"
                        data-original-title="Pinterest"
                        href="#"
                      >
                        <i className="fa fa-pinterest"></i>
                      </a>
                    </li>
                    <li className="rss">
                      <a
                        data-toggle="tooltip"
                        data-placement="top"
                        data-original-title="RSS"
                        href="#"
                      >
                        <i className="fa fa-rss"></i>
                      </a>
                    </li>
                    <li className="youtube">
                      <a
                        data-toggle="tooltip"
                        data-placement="top"
                        data-original-title="Youtube"
                        href="#"
                      >
                        <i className="fa fa-youtube-play"></i>
                      </a>
                    </li>
                    <li className="instagram">
                      <a
                        data-toggle="tooltip"
                        data-placement="top"
                        data-original-title="Instagram"
                        href="#"
                      >
                        <i className="fa fa-instagram"></i>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="main-header pt20 pb20">
          <div className="container">
            <div className="row">
              <div className="col-md-3 col-lg-3 col-sm-3 col-xs-12">
                <div className="logo">
                  <a href="/">
                    <img src="img/logo.png" alt="" />
                  </a>
                </div>
              </div>
              <div className="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                <div className="search-catagory">
                  <div className="categori">
                    <form
                      id="select-categoris"
                      method="post"
                      className="form-horizontal"
                    >
                      <select name="language" className="orderby">
                        <option defaultValue="selected">All Categories</option>
                        <option value="12">Clothing</option>
                        <option value="21">_Dresses </option>
                        <option value="25">_ _Coctail</option>
                        <option value="26">_ _Day </option>
                        <option value="27">_ _Evening </option>
                        <option value="28">_ _Sundresses</option>
                        <option value="29">_ _Sweters</option>
                        <option value="30">_ _Belts</option>
                        <option value="22">_Accessories</option>
                        <option value="31">_ _Hair Accessories </option>
                        <option value="32">_ _Hats </option>
                        <option value="33">_ _Gloves </option>
                        <option value="34">_ _Lifestyles</option>
                        <option value="35">_Medical</option>
                        <option value="36">_ _Phasellus</option>
                        <option value="23">_ _laptop </option>
                        <option value="37">_ _Phasellus</option>
                        <option value="38">_ _Healthcare </option>
                        <option value="39">_Electronic </option>
                        <option value="40">_ _Cosmetic </option>
                        <option value="13">_ _Electronics </option>
                        <option value="24">_ _camcorder </option>
                        <option value="46">_ _Healthcare </option>
                        <option value="47">_ _Laptop</option>
                        <option value="48">_ _Camera</option>
                        <option value="41">_ _Medical</option>
                        <option value="49">_ _Camera</option>
                        <option value="50">_ _Camcorders</option>
                        <option value="51">_ _Camcorders</option>
                        <option value="42">_ _Healthcare</option>
                        <option value="52">_ _Medical</option>
                        <option value="53">_ _Healthcare</option>
                        <option value="54">_ _Healthcare </option>
                        <option value="14">_Sports</option>
                        <option value="43">_ _category 1</option>
                        <option value="60">_ _day </option>
                        <option value="61">_ _evening </option>
                        <option value="62">_ _night </option>
                        <option value="44">_ _category 2 </option>
                        <option value="63">_kids </option>
                        <option value="64">_men </option>
                        <option value="65">_Women </option>
                        <option value="45">_ _category 3 </option>
                        <option value="66">_ _clothing </option>
                        <option value="67">_ _Accessories </option>
                        <option value="15">_ _Smartphone</option>
                        <option value="55">_ _category 1 </option>
                        <option value="56">_ _category 2 </option>
                        <option value="57">_ _category 3 </option>
                        <option value="16">_ _Beauty </option>
                        <option value="58">_ _category 1 </option>
                        <option value="59">_ _category 2 </option>
                        <option value="17">_ _Bags, Shoes</option>
                        <option value="18">_Networking </option>
                        <option value="19">_ _Accessories </option>
                        <option value="20">_ _Entertainment </option>
                      </select>
                      <div className="search-bar">
                        <SearchBar />
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <div className="col-md-5 col-lg-5 col-sm-5 col-xs-12">
                <div className="shop-wish-item">
                  <div className="top-links">
                    <ul className="list-inline">
                      <li className="dropdown menu-cart">
                        <a
                          data-toggle="dropdown"
                          className="dropdown-toggle
                    my_account"
                          title="add to cart"
                          href="#"
                        >
                          <i className="pe-7s-cart"></i>
                          <span>
                            {this.props.carts.length} item(s) -
                            {this.totalMoney(this.props.carts)}
                          </span>
                        </a>
                        <ul className="dropdown-menu dropdown-menu-right">
                          {this.props.carts.map((item, index) => (
                            <Cart item={item} index={index} />
                          ))}

                          <li>
                            <div className="cart_bottom">
                              <table className="minicart_total">
                                <tbody>
                                  <tr>
                                    <td className="text-left ">
                                      <span>Sub-Total</span>
                                    </td>
                                    <td className="text-right ">
                                      {this.props.totalPay}
                                    </td>
                                  </tr>
                                  <tr>
                                    <td className="text-left  last">
                                      <span>Total</span>
                                    </td>
                                    <td className="text-right  last">
                                      {this.props.totalPay}
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </li>
                          <li>
                            <div className="buttons">
                              <span className="cart_bt">
                                <Link className="btn" to="/Cart">
                                  Cart
                                </Link>
                              </span>
                              <span className="checkout_bt">
                                <a href="#" className="btn btn-blk">
                                  Checkout
                                </a>
                              </span>
                            </div>
                          </li>
                        </ul>
                      </li>

                      <li className="dropdown">
                        <a
                          data-toggle="dropdown"
                          className="dropdown-toggle
                    my_account"
                          title="My Account"
                          href="my-account.html"
                        >
                          <i className="pe-7s-user"></i>
                          <span>My Account</span>
                          <i className="fa fa-angle-down"></i>
                        </a>
                        <ul className="dropdown-menu dropdown-menu-right">
                          <li>
                            <a href="login.html">Register</a>
                          </li>
                          <li>
                            <a href="login.html">Login</a>
                          </li>
                        </ul>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Header.defaultProps = {
  carts: [],
  totalPay: 0
};
//tao component ProductCard, dua component vao list

const mapStateToProps = state => ({
  carts: state.carts,
  totalPay: state.totalPay
});

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators({}, dispatch)
});
export default connect(mapStateToProps, mapDispatchToProps)(Header);
