import React, { Component } from "react";
import { Link } from "react-router-dom";

class TopMenu extends Component {
  render() {
    return (
      <div>
        <Link to="/"> Home</Link>
        <br />
        <Link to="/list"> Product List</Link>
        <br />
        <Link to="/Cart">Cart</Link>
      </div>
    );
  }
}

export default TopMenu;
