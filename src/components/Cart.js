import React, { Component } from "react";

class Cart extends Component {
  render() {
    const { item, index, totalProduct } = this.props;
    return (
      <li>
        <div className="cart-img">
          <a href="#">
            <img width={90} height={76} src={item.item.images[0]} alt="" />
          </a>
        </div>
        <div className="cart-text">
          <a href="#">{item.item.name}</a>
          <div className="q-price">
            <p>
              <span>{item.item.final_price}</span>x{item.quanty}
            </p>
          </div>
        </div>
      </li>
    );
  }
}

export default Cart;
