import React, { Component } from "react";
import { Link } from "react-router-dom";

class Menu extends Component {
  render() {
    return (
      <div>
        {/* menu area start */}
        <div className="menu-area">
          <div className="container">
            <div className="row">
              <div className="col-md-12">
                <nav>
                  <ul>
                    <li>
                      <Link to="/">Home</Link>
                      <div className="mega-menu">
                        <div className="mega-catagory2">
                          <a className="version-cat" href="/">
                            <img src="img/home1.jpg" alt="" />
                            <h4>home version one</h4>
                          </a>
                        </div>
                        <div className="mega-catagory2">
                          <a className="version-cat" href="index-2.html">
                            <img src="img/home2.jpg" alt="" />
                            <h4>home version two</h4>
                          </a>
                        </div>
                        <div className="mega-catagory2">
                          <a className="version-cat" href="index-3.html">
                            <img src="img/home3.jpg" alt="" />
                            <h4>home version three</h4>
                          </a>
                        </div>
                        <div className="mega-catagory2">
                          <a className="version-cat" href="index-4.html">
                            <img src="img/home4.jpg" alt="" />
                            <h4>home version four</h4>
                          </a>
                        </div>
                        <div className="mega-catagory2">
                          <a className="version-cat" href="index-5.html">
                            <img src="img/home5.jpg" alt="" />
                            <h4>home version five</h4>
                          </a>
                        </div>
                      </div>
                    </li>
                    <li>
                      <a href="shop.html">SuperDeals</a>
                    </li>
                    <li>
                      <a href="shop.html">Featured Brands</a>
                    </li>
                    <li>
                      <a href="shop.html">Bestselling</a>
                    </li>
                    <li>
                      <a href="shop.html">Trending Styles</a>
                    </li>
                    <li>
                      <a href="blog.html">Blog</a>
                    </li>
                    <li>
                      <a href="#">Pages</a>
                      <ul className="mega-menu d-down">
                        <li>
                          <a href="about-us.html">About-Us</a>
                        </li>
                        <li>
                          <a href="contact.html">Contacu-Us</a>
                        </li>
                        <li>
                          <a href="blog.html">Blog</a>
                        </li>
                        <li>
                          <a href="blog-list.html">Blog-list</a>
                        </li>
                        <li>
                          <a href="single-blog.html">Single-Blog</a>
                        </li>
                        <li>
                          <a href="shop.html">Shop</a>
                        </li>
                        <li>
                          <a href="shop-list.html">Shop list</a>
                        </li>
                        <li>
                          <a href="single-product.html">Single-product</a>
                        </li>
                        <li>
                          <a href="checkout.html">Checkout</a>
                        </li>
                        <li>
                          <a href="cart.html">Cart</a>
                        </li>
                        <li>
                          <a href="wishlist.html">Wishlist</a>
                        </li>
                        <li>
                          <a href="login.html">login</a>
                        </li>
                        <li>
                          <a href="my-account.html">My-Account</a>
                        </li>
                        <li>
                          <a href="404.html">404</a>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
        {/* menu area end */}
        <div className="mobile-menu-area">
          <div className="container">
            <div className="row">
              <div className="col-sm-12">
                <nav id="dropdown">
                  <ul>
                    <li>
                      <a href="index.html">HOME</a>
                      <ul>
                        <li>
                          <a href="index.html">Home-01</a>
                        </li>
                        <li>
                          <a href="index-2.html">Home-02</a>
                        </li>
                        <li>
                          <a href="index-3.html">Home-03</a>
                        </li>
                        <li>
                          <a href="index-4.html">Home-04</a>
                        </li>
                        <li>
                          <a href="index-5.html">Home-05</a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a href="shop.html">SuperDeals</a>
                    </li>
                    <li>
                      <a href="shop.html">Featured Brands</a>
                    </li>
                    <li>
                      <a href="shop.html">Bestselling</a>
                    </li>
                    <li>
                      <a href="shop.html">Trending Styles</a>
                    </li>
                    <li>
                      <a href="blog.html">Blog</a>
                    </li>
                    <li>
                      <a href="#">PAGES</a>
                      <ul>
                        <li>
                          <a href="about-us.html">About-Us</a>
                        </li>
                        <li>
                          <a href="contact.html">Contacu-Us</a>
                        </li>
                        <li>
                          <a href="blog.html">Blog</a>
                        </li>
                        <li>
                          <a href="blog-list.html">Blog-list</a>
                        </li>
                        <li>
                          <a href="single-blog.html">Single-Blog</a>
                        </li>
                        <li>
                          <a href="shop.html">Shop</a>
                        </li>
                        <li>
                          <a href="shop-list.html">Shop list</a>
                        </li>
                        <li>
                          <a href="single-product.html">Single-product</a>
                        </li>
                        <li>
                          <a href="checkout.html">Checkout</a>
                        </li>
                        <li>
                          <a href="cart.html">Cart</a>
                        </li>
                        <li>
                          <a href="wishlist.html">Wishlist</a>
                        </li>
                        <li>
                          <a href="login.html">login</a>
                        </li>
                        <li>
                          <a href="my-account.html">My-Account</a>
                        </li>
                        <li>
                          <a href="404.html">404</a>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Menu;
