import React, { Component } from "react";
import { increase, removeItemInCart } from "../actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
class CartInPage extends Component {
  increaseItem = (id, isIncrease) => {
    this.props.increase(id, isIncrease);
    this.forceUpdate();
  };
  handleRemoveCart = (index) => {
    this.props.removeItemInCart(index);
  };
  render() {
    const { item, index } = this.props;
    return (
      <tr>
        <td class="name-img">
          <div class="th-img">
            <a href="#">
              <img src={item.item.images[0]} alt="" />
            </a>
          </div>
          <div class="name">
            <a href="#">{item.item.name}</a>
            <small>Reward Points: 700</small>
          </div>
        </td>
        <td class="model">product 11</td>
        <td>
          <span class="price">{item.item.final_price}</span>
        </td>
        <td>
          <div class="cart-quantity">
            <form action="#" method="POST">
              <div class="product-quantity">
                <div class="cart-quantity">
                  <div class="cart-plus-minus">
                    <div
                      class="dec qtybutton"
                      onClick={() => this.increaseItem(item.item.id, false)}
                    >
                      -
                    </div>
                    <input
                      type="text"
                      value={item.quanty}
                      name="qtybutton"
                      class="cart-plus-minus-box"
                    />
                    <div
                      class="inc qtybutton"
                      onClick={() => this.increaseItem(item.item.id, true)}
                    >
                      +
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </td>
        <td class="rem-up">
          <a
            href="#"
            data-toggle="tooltip"
            data-placement="top"
            title="Update!"
          >
            <i class="fa fa-repeat"></i>
          </a>
          <a data-toggle="tooltip" data-placement="top" title="Remove">
            <i
              onClick={() => this.handleRemoveCart(index)}
              class="fa fa-close"
            ></i>
          </a>
        </td>
        <td class="total">
          <span class="price">{item.quanty * item.item.final_price}</span>
        </td>
      </tr>
    );
  }
}

CartInPage.defaultProps = {
  product: undefined,
};

const mapStateToProps = (state) => ({
  product: state.product,
});

const mapDispatchToProps = (dispatch) => ({
  ...bindActionCreators({ increase, removeItemInCart }, dispatch),
});
export default connect(mapStateToProps, mapDispatchToProps)(CartInPage);
