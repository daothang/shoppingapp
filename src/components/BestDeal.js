import React, { Component } from "react";
import BestDealSingle from "./BestDealSingle";
class BestDeal extends Component {
  render() {
    const { bestDealProduct } = this.props;
    return (
      <div class="best-deals-area pt60">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="title pb30">
                <h1>Best Deals</h1>
              </div>
            </div>
          </div>
          <div class="row row-padd">
            {bestDealProduct != null
              ? bestDealProduct.map((item, index) => (
                  <BestDealSingle singleProduct={item} />
                ))
              : "Không có sản phẩm "}
          </div>
        </div>
      </div>
    );
  }
}

export default BestDeal;
