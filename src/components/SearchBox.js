import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { searchBox } from "../actions.js";
class SearchBox extends Component {
  constructor(props) {
    super(props);
    this.state = { value: "" };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  //   handleSubmit(event) {
  //     event.preventDefault();
  //     console.log(this.state.value);
  //
  //   }
  handleSubmit(event) {
    event.preventDefault();
    this.props.searchBox(this.state.value);
    console.log(this.state.value);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }
  render() {
    return (
      <div className="search-bar">
        <form onSubmit={this.handleSubmit}>
          <input
            type="text"
            placeholder="search hear..."
            value={this.state.value}
            onChange={this.handleChange}
          />
          <i className="fa fa-search">
            <input type="submit" value="" />
          </i>
        </form>
      </div>
    );
  }
}

SearchBox.defaultProps = {};
//tao component ProductCard, dua component vao list

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators({ searchBox }, dispatch)
});
export default connect(mapStateToProps, mapDispatchToProps)(SearchBox);
