import React, { Component } from "react";

class BigBanner extends Component {
  render() {
    return (
      <div class="big-banner-area pt20">
        <div class="container">
          <div class="discount-banner">
            <div class="mask-dis">
              <img alt="banner-dis" src="img/banner/banner_bigsale_home.jpg" />
            </div>
            <span class="info-dis">
              <img
                alt="sale-dis"
                src="img/banner/banner_bigsale_home_caption.png"
              />
            </span>
          </div>
        </div>
      </div>
    );
  }
}

export default BigBanner;
