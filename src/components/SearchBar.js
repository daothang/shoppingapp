import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getProducts, saveToQuery } from "../actions";

class SearchBar extends Component {
  state = { query: "" };
  handleSearch = event => {
    event.preventDefault();
    this.props.getProducts(this.state.query, 1, true);
    this.props.saveToQuery(this.state.query);
  };
  handleChange = event => {
    this.setState({ query: event.target.value });
  };

  render() {
    const { errMsg, status } = this.props;
    return (
      <nav className="navbar navbar-light bg-red">
        <form onSubmit={this.handleSearch}>
          <input
            className="form-control mr-sm-2"
            type="search"
            placeholder="Search"
            aria-label="Search"
            value={this.state.query}
            onChange={this.handleChange}
          />
          <button className="btn btn-light my-2 my-sm-0" type="submit">
            Search
          </button>
        </form>
        {status == "START" && <p>Loading</p>}
        <h4>{this.props.errMsg}</h4>
      </nav>
    );
  }
}

SearchBar.defaultProps = {};

const mapStateToProps = state => ({
  products: state.products,
  errMsg: state.errMsg,
  status: state.status
});

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators({ getProducts, saveToQuery }, dispatch)
});
export default connect(mapStateToProps, mapDispatchToProps)(SearchBar);
