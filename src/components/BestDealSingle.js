import React, { Component } from "react";
class BestDealSingle extends Component {
  render() {
    const { singleProduct } = this.props;
    return (
      <div class="col-md-4 col-sm-6 col-padd ">
        {console.log("xin chao ba con")}
        <div class="single-product">
          <div class="product-img">
            <a href="#">
              <img src="img/product/h9-380x320.jpg" alt="" />
            </a>
            <div class="time-ratting-bought">
              <div class="bought">
                <span>11 Bought</span>
              </div>
              <div class="time-counter">
                <div data-countdown="2018/07/01"></div>
              </div>
            </div>
            <div class="best-deal-tag"></div>
          </div>
          <div class="single-product-text">
            <div class="product-name">
              <a href="#">{singleProduct.name}</a>
            </div>
            <div class="dis-percentage">
              <span>90%</span>
            </div>
            <div class="price-type">
              <div class="old-price">
                <span>
                  <del>$800</del>
                </span>
              </div>
              <div class="running-price">
                <span>$600</span>
              </div>
            </div>
            <div class="cart-btn">
              <a href="#">Add To Cart</a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default BestDealSingle;
