import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { addCart } from "../actions";
import { Link } from "react-router-dom";

class SingleProduct extends Component {
  saleOff = (price, finalPrice) => {
    let sale = 100 - (finalPrice / price) * 100;
    return sale.toFixed(1) + "%";
  };
  componentDidUpdate() {}

  filterPrice = (limitPrice, product) => {
    let display = false;
    if (
      typeof limitPrice == "object" &&
      product.final_price > limitPrice.selectedOption.value
    )
      display = true;
    return display;
  };

  addToCart = (id) => {
    this.props.addCart(id);
  };
  render() {
    const { product, index, limitPrice } = this.props;
    return (
      <div
        class="col-md-4 col-sm-6 col-padd "
        style={{
          display: this.filterPrice(limitPrice, product) ? "none" : "block",
        }}
      >
        <div class="single-product">
          <Link to={`/chi-tiet?id=${product.id}`}>
            <div class="product-img">
              <a href="#">
                <img src={product.img_url} alt="" />
              </a>
              <div class="time-ratting-bought">
                <div class="bought">
                  <span>11 Bought</span>
                </div>
                <div class="time-counter">
                  <div data-countdown="2018/07/01"></div>
                </div>
              </div>
              <div class="best-deal-tag"></div>
            </div>
          </Link>
          <div class="single-product-text">
            <Link to={`/chi-tiet?id=${product.id}`}>
              <div class="product-name">
                <a href="#">{product.name}</a>
              </div>
            </Link>
            <div class="dis-percentage">
              <span>{this.saleOff(product.price, product.final_price)}</span>
            </div>
            <div class="price-type">
              <div class="old-price">
                <span>
                  <del>{product.price}</del>
                </span>
              </div>
              <div class="running-price">
                <span>{product.final_price}</span>
              </div>
            </div>

            <div class="cart-btn">
              <button
                className="btn btn-primary c-btn"
                type="button"
                onClick={() => this.addToCart(product.id)}
              >
                Add to Cart
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

SingleProduct.defaultProps = { limitPrice: undefined };

const mapStateToProps = (state) => ({ limitPrice: state.limitPrice });

const mapDispatchToProps = (dispatch) => ({
  ...bindActionCreators({ addCart }, dispatch),
});
export default connect(mapStateToProps, mapDispatchToProps)(SingleProduct);
