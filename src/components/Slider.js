import React, { Component } from "react";
import { Carousel } from "react-bootstrap";
import { Link } from "react-router-dom";

class Slider extends Component {
  render() {
    return (
      <div class="slider-area pt20">
        <div class="container">
          <div class="row">
            <div class="col-md-3 col-sm-3">
              <div class="catagori-menu">
                <ul>
                  <li>
                    <Link to={`/sanpham?keywork=Quan ao nu`}>
                      <span>
                        <img src="img/icon/icon2-20x25.png" alt="" />
                      </span>
                      Quần áo nữ
                    </Link>
                  </li>
                  <li>
                    <Link to={`/sanpham?keywork=Quan ao nam`}>
                      <span>
                        <img src="img/icon/icon3-20x25.png" alt="" />
                      </span>
                      Quần áo nam
                    </Link>
                  </li>
                  <li>
                    <Link to={`/sanpham?keywork=nha&&vuon`}>
                      <span>
                        <img src="img/icon/icon4-20x25.png" alt="" />
                      </span>
                      Nhà & Vườn
                    </Link>
                  </li>
                  <li>
                    <a href="shop.html">
                      <span>
                        <img src="img/icon/icon5-20x25.png" alt="" />
                      </span>
                      Túi & Giày
                    </a>
                  </li>
                  <li>
                    <a href="shop.html">
                      <span>
                        <img src="img/icon/icon6-20x25.png" alt="" />
                      </span>
                      Thức ăn
                    </a>
                  </li>
                  <li>
                    <a href="shop.html">
                      <span>
                        <img src="img/icon/icon7-20x25.png" alt="" />
                      </span>
                      Sức khỏe & Làm đẹp
                    </a>
                  </li>
                  <li>
                    <a href="shop.html">
                      <span>
                        <img src="img/icon/icon8-20x25.png" alt="" />
                      </span>
                      Máy tính & Văn phòng
                    </a>
                  </li>
                  <li>
                    <a href="shop.html">
                      <span>
                        <img src="img/icon/icon10-20x25.png" alt="" />
                      </span>
                      Thể thao & Ngoài trời
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="col-md-9 col-sm-9 pl5">
              {/* main slider start  */}
              <div class="main-slider">
                <Carousel>
                  <Carousel.Item>
                    <img
                      className="d-block w-100"
                      src="img/slider/slideshow1.jpg"
                      alt="First slide"
                    />
                    <Carousel.Caption>
                      <h3>First slide label</h3>
                      <p>
                        Nulla vitae elit libero, a pharetra augue mollis
                        interdum.
                      </p>
                    </Carousel.Caption>
                  </Carousel.Item>
                  <Carousel.Item>
                    <img
                      className="d-block w-100"
                      src="img/slider/slideshow2.jpg"
                      alt="Third slide"
                    />

                    <Carousel.Caption>
                      <h3>Second slide label</h3>
                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      </p>
                    </Carousel.Caption>
                  </Carousel.Item>
                  <Carousel.Item>
                    <img
                      className="d-block w-100"
                      src="img/slider/slideshow3.jpg"
                      alt="Third slide"
                    />

                    <Carousel.Caption>
                      <h3>Third slide label</h3>
                      <p>
                        Praesent commodo cursus magna, vel scelerisque nisl
                        consectetur.
                      </p>
                    </Carousel.Caption>
                  </Carousel.Item>
                </Carousel>
              </div>
              {/* main slider slider */}
              <div class="slider-banner">
                <a href="#">
                  <img src="img/banner/banner-slider1.jpg" alt="" />
                </a>
                <a href="">
                  <img src="img/banner/banner-slider2.jpg" alt="" />
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Slider;
