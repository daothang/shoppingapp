import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {  addCart } from "../actions";
import { Link } from "react-router-dom";
import "./ProductCard.css";

class ProductCard extends Component {
  state = {
    isDisplayed: false
  };
  showPrice = () => {
    this.setState({ isDisplayed: true });
  };
  addToCart = (id) => {
   
    this.props.addCart(id);
  };
  render() {
    const { product, index } = this.props;
    return (
      <div className="product">
        <Link
          to={`/detail?id=${product.id}&name=${product.name}&index=${index}`}
        >
          <div>
            <img src={product.img_url} alt={product.name}></img>
            <h4
              style={{
                textAlign: "center",
                backgroundColor: this.props.isSeleted && "red"
              }}
            >
              {product.name}
            </h4>
          </div>
        </Link>
        <button type="button" onClick={this.showPrice}>
          {" "}
          Show price
        </button>
        <button type="button" onClick={()=>this.addToCart(product.id)}>Add to Cart</button>
        
        <h5
          style={{
            textAlign: "center",
            display: this.state.isDisplayed ? "block" : "none"
          }}
        >
          {product.price}
        </h5>
      </div>
    );
  }
}



ProductCard.defaultProps = {};
//tao component ProductCard, dua component vao list

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators({ addCart }, dispatch)
});
export default connect(mapStateToProps, mapDispatchToProps)(ProductCard);
