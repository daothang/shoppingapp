import { makeProductsApi, makeProductdetailApi } from "./apis";

export const FILTER = "FILTER";
export const ADD_TO_CART = "ADD_TO_CART";
export const REMOVE_FROM_CART = "REMOVE_FROM_CART";
export const INCREASE_ITEM = "INCREASE_ITEM";
export const GET_PRODUCTS = "GET_PRODUCTS";
export const GET_PRODUCTS_START = "GET_PRODUCTS_START";
export const GET_PRODUCTS_SUCCESS = "GET_PRODUCTS_SUCCESS";
export const GET_PRODUCT = "GET_PRODUCT";
export const GET_PRODUCTS_FAIL = "GET_PRODUCTS_FAIL";
export const GET_PRODUCT_ID = "GET_PRODUCT_ID";
export const SAVE_TO_QUERY = "SAVE_TO_QUERY";

export const LOGIN_START = "LOGIN_START";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAIL = "LOGIN_FAIL";
const PRODUCTS_API = "https://mapi.sendo.vn/mob/product/search?p=1&q=quan+ao";

export const increaseItem = (id, isIncrease) => ({
  type: INCREASE_ITEM,
  id: id,
  isIncrease: isIncrease,
});
export const rmItemInCart = (index) => ({
  type: REMOVE_FROM_CART,
  index: index,
});
export const saveToQuery = (query) => ({
  type: SAVE_TO_QUERY,
  query: query,
});
export const addToCart = (item) => ({
  type: ADD_TO_CART,
  item: convertSendo(item),
});
export const filter = (category) => ({
  type: FILTER,
  category: category,
});
export const getProduct = (index) => ({
  type: GET_PRODUCT,
  index,
});
export const getProducSstart = () => ({
  type: GET_PRODUCTS_START,
});

export const getProductsFail = (errorMsg) => ({
  type: GET_PRODUCTS_FAIL,
  errorMsg,
});
export const getProductSuccess = (products, page, isNewSearch) => ({
  type: GET_PRODUCTS_SUCCESS,
  products,
  page,
  isNewSearch,
});

export const getSendoProductSuccess = (sendoProduct) => ({
  type: GET_PRODUCT_ID,
  sendoProduct: convertSendo(sendoProduct),
});

export const loginStart = (object) => ({
  type: LOGIN_START,
  object,
});
export const loginSuccess = (object) => ({
  type: LOGIN_SUCCESS,
  object,
});
export const loginFail = (object) => ({
  type: LOGIN_FAIL,
  object,
});
export const getProducts = (query, page = 1, isNewSearch = false) => {
  return (dispatch) => {
    console.log("queerry " + query);
    fetch(makeProductsApi(page, query))
      .then((res) => res.json())
      .then((json) => {
        //dispatch
        dispatch(getProductSuccess(json.data, page, isNewSearch));
      })
      .catch((err) => {
        console.log(err);
        dispatch(getProductsFail(err.message));
      });
  };
};
export const getSendoProduct = (id) => {
  return (dispatch) => {
    fetch(makeProductdetailApi(id))
      .then((res) => res.json())
      .then((json) => {
        //dispatch
        dispatch(getSendoProductSuccess(json));
      })
      .catch((err) => console.log(err));
  };
};

var convertSendo = (sendoProduct) => {
  let images = [];
  sendoProduct.images.map((item) => {
    images.push(`https://media3.scdn.vn/${item}`);
  });
  // for (let i = 0; i < sendoProduct.images.length; i++) {
  //   sendoProduct.images[i] = `https://media3.scdn.vn/${sendoProduct.images[i]}`;
  // }
  sendoProduct.images = images;
  return sendoProduct;
};

export const searchBox = (value, page = 1) => {
  return (dispatch) => {
    fetch(makeProductsApi(page, value))
      .then((res) => res.json())
      .then((json) => {
        //dispatch
        dispatch(getProductSuccess(json.data));
      })
      .catch((err) => {
        console.log(err);
        dispatch(getProductsFail(err.message));
      });
  };
};

export const login = (username, passsword) => {
  return (dispatch) => {
    fetch("LOGIN_API", { method: "post" })
      .then((res) => res.json())
      .then((json) => {
        dispatch(loginSuccess());
      })
      .catch((err) => {
        console.log(err);
        dispatch(getProductsFail(err.message));
      });
  };
};
export const addCart = (id) => {
  return (dispatch) => {
    fetch(makeProductdetailApi(id))
      .then((res) => res.json())
      .then((json) => {
        dispatch(addToCart(json));
      })
      .catch((err) => {
        console.log(err);
        dispatch(getProductsFail(err.message));
      });
  };
};
export const increase = (id, isIncrease = false) => {
  return (dispatch) => {
    dispatch(increaseItem(id, isIncrease));
  };
};

export const removeItemInCart = (index) => {
  return (dispatch) => {
    dispatch(rmItemInCart(index));
  };
};
