import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import DetailPage from "./pages/DetailPage";
import AuthPage from "./pages/AuthPage";
import Cart from "./pages/Cart";
import Header from "./components/Header";
import Menu from "./components/Menu";
import Home from "./pages/Home";
import ListProducts from "./pages/ListProducts";
import ProductDetailPage from "./pages/ProductDetailPage";

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Router>
          <Header />
          <Menu />
          <Switch>
            <Route exact path="/Cart" component={Cart}></Route>
            <Route exact path="/" component={Home}></Route>
            <Route exact path="/login" component={AuthPage}></Route>
            <Route exact path="/sanpham" component={ListProducts}></Route>
            <Route exact path="/detail" component={DetailPage}></Route>
            <Route exact path="/chi-tiet" component={ProductDetailPage}></Route>
          </Switch>
        </Router>
      </React.Fragment>
    );
  }
}

export default App;
