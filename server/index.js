const express = require("express");
const app = express();
app.get("/", (req, res) => {
  res.addListener("hello world!");
});
app.listeners(4000, () => {
  console.log("server is running on 4000");
});
